更新日志

2020-05-27编译OpenCore-0.5.9-05-27

- 例行更新
- 优化部分代码

2020-05-25编译OpenCore-0.5.9-05-25

- 例行更新
- 优化部分代码

2020-05-23编译OpenCore-0.5.9-05-23

- 优化部分代码

2020-05-21编译OpenCore-0.5.9-05-21

-  新增扫描策略对PCI设备的支持(如VIRTIO)

2020-05-19编译OpenCore-0.5.9-05-19

- 新增MacBookPro16,2和MacBookPro16,3模版

2020-05-17编译OpenCore-0.5.9-05-17

- 将`ACPI`,`DeviceProperties`, 和 `NVRAM`模块中的 `（阻止）Block` 改名为 `（删除）Delete` 

2020-05-15编译OpenCore-0.5.9-05-15

- 修复 `FadtEnableReset` FACP表单过小
- 修复QEMU 5.0 和 KVM accelerator导致CPU崩溃
- 移除 `Config-UEFI-Quirks-RequestBootVarFallback`
- 新增 `Config-UEFI-Quirks-DeduplicateBootOrder` 
- 移除`Config-UEFI-Output-DirectGopCacheMode`
- 修复日志耗尽导致引导失败
- 修复文本模式时控制台失效
- 修复blit-only GOP的兼容性
- 修复 在DeviceProperty 和 NVRAM `Block`中“#”失效

2020-05-13编译OpenCore-0.5.9-05-11

- 更新了最新的Resources

2020-05-11编译OpenCore-0.5.9-05-11

- 当图形界面external故障或错误时，强制使用文字builtin界面
- 修复NVRAM变量为空时的警告(如rtc-blacklist)
- 新增 `ApplePanic` 在ESP根目录下储存崩溃日志
- 修复`ReconnectOnResChange` 发生资源变化时，重新连接
- 修复OpenCanopy使用图形界面时的错误
- 修复OpenCanopy文字显示错误
- 添加OpenCanopy部分快捷键的支持(例如Ctrl+Enter)
- 增加builtin文本模式的兼容性

2020-05-07编译OpenCore-0.5.9-05-07

- 在`OpenCanopy`中添加了对HiDPI的完整支持
- 通过使用`CoreText`改进字体渲染
- 修复灯光与自定义背景时字体的渲染
- 在启动菜单列表中添加了`Boot####`选项
-  移除 `HideSelf` 对 `BOOTx64.efi`的识别
- 新增 `BlacklistAppleUpdate` 限制 Apple FW 更新
- 修复accidental工具和NVRAM的默认重启设置
- 修复无法识别`com.apple.recovery.boot` 引导菜单
- 改善了 NVRAM 重置后无法删除 `BootProtect`启动项的问题
- 禁用picker UI时提高引导性能

2020-05-05编译OpenCore-0.5.9-05-05

- 更新版本号

2020-05-04官方编译OpenCore-0.5.8正式版

- 修正了创建vault时校验和检查无效的问题。

2020-05-03编译OpenCore-0.5.8-05-03

- 优化了部分代码
- 更新部分固件

2020-05-01编译OpenCore-0.5.8-05-01

- 优化了部分代码

2020-04-29编译OpenCore-0.5.8-04-29

- 在OpenCanopy中增加了部分HiDPI的支持
- 优化了部分代码

2020-04-27编译OpenCore-0.5.8-04-27

- 修复处理24-bit分辨率的问题
- 新增用于DuetPkg的`Ps2KeyboardDxe`驱动
- 更新了`BootInstall` 的DuetPkg版本(开源)

2020-04-25编译OpenCore-0.5.8-04-25

- 新增Misc- Security-BootProtect项
- 安装10.8时，注入固有的kext
- 新增使用图形界面时，对OpenCanopy超时的支持

2020-04-23编译OpenCore-0.5.8-04-23

- 固件版本更新
- 优化部分代码

2020-04-21编译OpenCore-0.5.8-04-21

- 将`Config-UEFI-协议（Protocols）`重命名为`Config-UEFI-ProtocolOverrides(协议覆盖)`，以便于识别
- 新增`Config-Misc-Tools-ResetSystem`工具，可以在菜单中显示关机/重启

2020-04-19编译OpenCore-0.5.8-04-19

- 增加了支持保留内存区域
- 增加RtcRw工具来调整RTC内存
- 添加热补丁applertcchecksum防止内核崩溃
- 添加AppleRtcRam协议

2020-04-17编译OpenCore-0.5.8-04-17

- 修复了AppleEvent和OpenCanopy对OVMF TPL的兼容性限制
- 添加了支持OVMF的鼠标驱动到驱动包。

2020-04-15编译OpenCore-0.5.8-04-15

- 在OpenCanopy中添加了AppleEvent鼠标支持
- 优化部分代码

2020-04-13编译OpenCore-0.5.8-04-13

- 固件版本更新，新增MBA91
- 优化部分代码
- 新增`Config-UEFI-APFS`项，用于APFS驱动和增强安全性（即不用Config-UEFI-Drivers里面的ApfsDriverLoader）

2020-04-10编译OpenCore-0.5.8-04-10

- 修复不正确的实用程序和资源安装包
- 修正`Custom` `UpdateSMBIOSMode` 修改SMBIOSv3列表的问题
- 通过`UpdateSMBIOSMode`使用更新的文档来覆盖分离SMBIOS
- 修复`OpenCanopy`中macOS 10.15.4 安装图标的问题

2020-04-07编译OpenCore-0.5.8-04-07(更新版本号)

2020-04-06编译OpenCore-0.5.7官方正式版

- 将`Config-Misc-Boot-PickerAttributes`改名为`Config-Misc-Boot-ConsoleAttributes`
- 为UI配置添加`Config-Misc-Boot-PickerAttributes`

2020-04-05编译OpenCore-0.5.7-04-05

- 添加 `config-Booter-Quirks-ProtectMemoryRegions`以修复内存加载时的一些问题。
- 移除`config-Booter-Quirks-ProtectCsmRegion`用`config-Booter-Quirks-ProtectMemoryRegions`代替

2020-04-03编译OpenCore-0.5.7-04-03

- 修复了`OpenRuntime`中4K对齐，以完善在SKL上启动Linux
- 添加`config-Booter-Quirks-SyncRuntimePermissions`项，修复在CFL+上启动Linux
- 添加`config-Booter-Quirks-RebuildAppleMemoryMap`项，修复戴尔5490上启动macOS的问题
- 删除`config-Booter-Quirks-ShrinkMemoryMap`项，添加更高级的`config-Booter-Quirks-RebuildAppleMemoryMap`项
- 使用新系统(SKL+)时不用勾选`EnableWriteUnprotector`项，最好还是勾选
- 添加`Config-Misc-Debug-AppleDebug`，勾选后 boot.efi调试日志保存到OpenCore日志中，一般不勾选此参数仅适用于10.15.4及以上的版本

2020-03-30编译OpenCore-0.5.7-03-30

- 修复了10.15.4电源超时导致的内核崩溃
- 修复了OpenRuntime中4K对齐

2020-03-29编译OpenCore-0.5.7-03-29

- 优化部分代码

2020-03-24编译OpenCore-0.5.7-03-24

- 优化部分代码

2020-03-22编译OpenCore-0.5.7-03-22

- 继续更新内置固件（主要是机型）
- 优化部分代码

2020-03-20编译OpenCore-0.5.7-03-20

- 更新内置固件（主要是机型）

2020-03-18编译OpenCore-0.5.7-03-18

- 重写' readlabel '使应用程序支持' disklabel '的编码。
- 将“FwRuntimeServices”改名为“OpenRuntime”。
- 将“AppleUsbKbDxe”改名为“OpenUsbKbDxe”。
- 将“BootLiquor.efi”改名为“OpenCanopy.efi”。

2020-03-14编译OpenCore-0.5.7-03-14，

- 用“Windows”替换“BOOTCAMP Windows”。
- 在Tools中添加OpenCoreShell提供的`OpenShell.efi` 

2020-03-12编译OpenCore-0.5.7-03-12，<font color= "#FF0000" >添加ProtectUefiServices项，用于修复Z390在DevirtualiseMmio上的问题（03-12新增）</font>

2020-03-11编译OpenCore-0.5.7-03-11，优化部分代码

2020-03-08编译OpenCore-0.5.7-03-09,添加了、修复了7个项目

2020-03-08编译OpenCore-0.5.7-03-08,优化部分代码

2020-03-03编译OpenCore-0.5.7-03-03(更新版本号)

2020-03-02官方编译OpenCore-0.5.6-03-02

2020-02-24编译OpenCore-0.5.6添加开机提示音、为10.13以上的版本在boot.efi 中增加音频支持

2020-02-23编译OpenCore-0.5.6优化2个项目

2020-02-19编译OpenCore-0.5.6更新说明文件

2020-02-16-编译OpenCore-0.5.6重大调整版

2020-02-03-编译OpenCore-0.5.6

2020-01-15-编译OpenCore-0.5.5

2019-12-24-编译OpenCore-0.5.4

2019-12-19-编译OpenCore-0.5.3

特别是不能在远景论坛转载！可能被远景因此封号，谢谢合作

黑苹果OpenCore开放群，群号:9422866，注明“独行秀才Blog引入”

具体使用请参阅[OpenCore配置文字说明第四版（基于04月17日编译版）](https://shuiyunxc.gitee.io/2020/03/10/instru/index/)